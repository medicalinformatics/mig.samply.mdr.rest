# Samply MDR REST

Samply MDR REST is the REST Interface to the Samply MDR.

This project implements the REST Interface *only*.

## Installation

See the MDR Bundle install guide.

## Usage

Open your web browser and navigate to the application, e.g.
`http://localhost:8080/rest/api/mdr/`.

## Technology

This project uses the following technologies:

* Jersey 2.13 for the REST Interface

## Build

Use maven to build the `war` file:

```
mvn clean package
```
