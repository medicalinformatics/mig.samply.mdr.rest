# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.4.0] - 2020-06-23
### Added
- Concept Association added to staging import

## [4.3.0] - 2020-06-19
### Added
- REST API endpoint for retrieving recommendations for a given search string and optionally a language
- Staging import also accepts JSON now
### Changed
- Error messages for numerical ranges now include the datatype (integer / float)
### Fixed
- Encoding was wrong in German messages file
- Typos in error messages

## [4.2.2] - 2019-08-06
### Fixed
- Return correct definition/designation for root node when getting its children

## [4.2.1] - 2019-08-06
### Fixed
- Getting catalogue nodes was erroneously filtering single subnodes with children

## [4.2.0] - 2019-07-25
### Changed
- Getting filtered catalogs accepts the search term via path param instead of header param
### Deprecated
- Method to get whole catalogs no longer marked as deprecated

## [4.1.0] - 2019-06-26
### Added
- Method to get codes by their value instead of urn

## [4.0.3] - 2019-06-14
### Deprecated
- Method to get whole catalogs marked as deprecated
### Fixed
- Getting allowed subcodes for whole catalogs was very slow for large catalogs

## [4.0.2] - 2019-06-14
### Fixed
- Subcode list was empty

## [4.0.1] - 2019-06-14
### Fixed
- Codes with children that are not permitted in the given value domain are correctly filtered out

## [4.0.0] - 2019-06-13
### Added
- CodeDTO contains "hasSubcodes" attribute (might break compatibility with mdrclient or mdrfaces)
- Accept staged elements in XML format
- Concept association for dataelements
- Optional header param "query" when getting catalogs

## [3.3.0] - 2019-02-08
### Added
- Initial release