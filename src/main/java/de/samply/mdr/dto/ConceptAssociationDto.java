/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dto;

import de.samply.mdr.dal.dto.ConceptAssociation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import de.samply.mdr.dal.jooq.enums.RelationType;
import javax.xml.bind.annotation.XmlAttribute;

public class ConceptAssociationDto {

  /**
   * The system for this concept association.
   */
  private String system;

  /**
   * The source for this concept association.
   */
  private Integer sourceId;

  /**
   * The version for this concept association.
   */
  private String version;

  /**
   * The term for this concept association.
   */
  private String term;

  /**
   * The text for this concept association.
   */
  private String text;

  /**
   * The linktype for this concept association.
   */
  private RelationType linktype;

  public static Collection<ConceptAssociationDto> convert(
      List<ConceptAssociation> conceptAssociationList) {
    Collection<ConceptAssociationDto> target = new ArrayList<>();
    for (ConceptAssociation conceptAssociation : conceptAssociationList) {
      target.add(convert(conceptAssociation));
    }
    return target;
  }

  private static ConceptAssociationDto convert(ConceptAssociation ConceptAssociation) {
    ConceptAssociationDto dto = new ConceptAssociationDto();
    dto.setSystem(ConceptAssociation.getSystem());
    dto.setSourceId(ConceptAssociation.getSourceId());
    dto.setVersion(ConceptAssociation.getVersion());
    dto.setTerm(ConceptAssociation.getTerm());
    dto.setText(ConceptAssociation.getText());
    dto.setLinktype(ConceptAssociation.getLinktype());
    return dto;
  }

  @XmlAttribute(name = "concept_system")
  public String getSystem() {
    return system;
  }

  public void setSystem(String system) {
    this.system = system;
  }

  @XmlAttribute(name = "concept_source")
  public Integer getSourceId() {
    return sourceId;
  }

  public void setSourceId(Integer sourceId) {
    this.sourceId = sourceId;
  }

  @XmlAttribute(name = "concept_version")
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  @XmlAttribute(name = "concept_term")
  public String getTerm() {
    return term;
  }

  public void setTerm(String term) {
    this.term = term;
  }

  @XmlAttribute(name = "concept_text")
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @XmlAttribute(name = "concept_linktype")
  public RelationType getLinktype() {
    return linktype;
  }

  public void setLinktype(RelationType linktype) {
    this.linktype = linktype;
  }
}
