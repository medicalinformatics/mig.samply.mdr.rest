/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dto;

import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The data transfer object for elements. Used in /members
 */
@XmlRootElement
public class ResultDto {

  /**
   * The id (= urn) of the result
   */
  private String id;

  /**
   * The element type of the result
   */
  private Elementtype type;

  /**
   * The identification object
   */
  private IdentificationDto identification;

  /**
   * The definitions of the result.
   */
  private Collection<DefinitionDto> definitions;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
    type = ScopedIdentifier.fromUrn(id).getElementType();
  }

  @XmlElement(name = "designations")
  public Collection<DefinitionDto> getDefinitions() {
    return definitions;
  }

  public void setDefinitions(Collection<DefinitionDto> definitions) {
    this.definitions = definitions;
  }

  public Elementtype getType() {
    return type;
  }

  public void setType(Elementtype type) {
    this.type = type;
  }

  /**
   * @return the identification
   */
  public IdentificationDto getIdentification() {
    return identification;
  }

  /**
   * @param identification the identification to set
   */
  public void setIdentification(IdentificationDto identification) {
    this.identification = identification;
  }

}
