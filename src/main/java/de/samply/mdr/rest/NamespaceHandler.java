/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.samply.mdr.json.JsonConverter;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ImportDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.StagingDao;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dto.DataElementSearchDto;
import de.samply.mdr.dto.NamespaceDto;
import de.samply.mdr.dto.ResultDto;
import de.samply.mdr.staging.utils.ImportToStagingConverter;
import de.samply.mdr.xsd.staging.ImportType;
import de.samply.mdr.xsd.staging.StagedElementType;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.jooq.ConnectionRunnable;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles all namespace requests
 */
@Path("/namespaces")
public class NamespaceHandler extends AbstractHandler {

  private static final Logger logger = LoggerFactory.getLogger(NamespaceHandler.class);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Collection<NamespaceDto> getNamespaces() throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<DescribedElement> readable = NamespaceDao.getReadableNamespaces(ctx, user.getId());
      List<DescribedElement> writable = NamespaceDao.getWritableNamespaces(ctx, user.getId());
      return convert(readable, writable);
    }
  }

  @Path("/{namespace}/members")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto getRootElements(@PathParam("namespace") String namespace)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Collection<ResultDto> elements = convert(
          IdentifiedDao.getRootElements(ctx, user.getId(), namespace));
      DataElementSearchDto dto = new DataElementSearchDto();
      dto.setTotalCount(elements.size());
      dto.setResults(elements);
      return dto;
    }
  }

  @Path("/{namespace}/search")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto search(@PathParam("namespace") String namespace,
      @QueryParam("query") String query, @QueryParam("type") List<String> types)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<Elementtype> typeList = new ArrayList<>();
      for (String type : types) {
        typeList.add(Elementtype.valueOf(type));
      }
      DataElementSearchDto dto = new DataElementSearchDto();
      Collection<ResultDto> results = convert(IdentifiedDao
          .findElements(ctx, user.getId(), query, namespace,
              typeList.toArray(new Elementtype[types.size()]), Status.values(),
              new HashMap<String, String>()));
      dto.setResults(results);
      dto.setTotalCount(results.size());
      return dto;
    }
  }

  @POST
  @Path("/{namespace}")
  @Consumes({MediaType.APPLICATION_JSON})
  public Response stageJsonElement(@PathParam("namespace") String namespaceName, String object) {
    Gson gson = new Gson();
    JsonObject jsonObject = gson.fromJson(object,JsonObject.class);

    //TODO: new JsonValidator().validationService(jsonObject);
    JsonConverter jsonConverter = new JsonConverter();
    ImportType importType;

    try {
      importType = jsonConverter.converter(jsonObject);
    } catch (JAXBException | TransformerException | ParserConfigurationException | IOException e) {
      logger.error("Not a valid input to json Schema " + e);
      return Response.status(Response.Status.BAD_REQUEST).build();
    }
    return stageElement(namespaceName, importType);
  }

  @POST
  @Path("/{namespace}")
  @Consumes({MediaType.APPLICATION_XML})
  public Response stageXmlElement(@PathParam("namespace") String namespaceName, ImportType xmlImport) {
   //TODO Validation
    return stageElement(namespaceName,xmlImport);
  }

  public Response stageElement (String namespaceName, ImportType xmlImport){
    int namespaceId;
    Map<Integer, Integer> idMap = new HashMap<>();

    // Don't allow empty imports
    if (xmlImport == null || xmlImport.getStagedElement().isEmpty()) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    if (user.getUsername() == null) {
      return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    int userId = user.getId();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(new ConnectionRunnable() {
        @Override
        public void run(Connection c) throws Exception {
          c.setAutoCommit(false);
        }
      });
      try {
        DescribedElement namespace = NamespaceDao.getNamespace(ctx, userId, namespaceName);
        if (namespace != null) {
          namespaceId = namespace.getElement().getId();
        } else {
          // Either namespace not found, or user can't read it
          return Response.status(Response.Status.NOT_FOUND).build();
        }
      } catch (DataAccessException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity("namespace not found").build();
      }

      Import _import = new Import();
      _import.setNamespaceId(namespaceId);
      _import.setSource("xml");
      _import.setCreatedBy(userId);
      _import.setLabel(xmlImport.getLabel());
      int importId = ImportDao.saveImport(ctx, _import);

      for (StagedElementType stagedElement : xmlImport.getStagedElement()) {
        // Don't allow dataelements without validation
        if (stagedElement.getElementType().equals(de.samply.mdr.xsd.staging.ElementType.DATAELEMENT)
            && stagedElement.getValidation() == null) {
          logger.warn("Element without validation. Skipping.");
          continue;
        }

        // Don't allow dataelementgroups without members
        if (stagedElement.getElementType()
            .equals(de.samply.mdr.xsd.staging.ElementType.DATAELEMENTGROUP)
            && (stagedElement.getMembers() == null ||
            stagedElement.getMembers().getStagedElement() == null ||
            stagedElement.getMembers().getStagedElement().size() == 0)) {
          logger.warn("Group without members. Skipping.");
          continue;
        }

        Map<Integer, List<Staging>> stagingDTOs = ImportToStagingConverter
            .xmlStagedelementToStaging(stagedElement, importId);
        Iterator iterator = stagingDTOs.entrySet().iterator();
        while (iterator.hasNext()) {
          Map.Entry<Integer, List<Staging>> pair = (Map.Entry<Integer, List<Staging>>) iterator
              .next();
          StagingDao.saveStagedElements(ctx, pair.getValue(), idMap);
          iterator.remove();
        }
      }
      ctx.connection(new ConnectionRunnable() {
        @Override
        public void run(Connection connection) throws Exception {
          connection.commit();
        }
      });

    } catch (DataAccessException e) {
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    } catch (IllegalArgumentException e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getCause()).build();
    }
    logger.info("Inserted elements with ids " + idMap.entrySet());
    return Response.noContent().build();
  }

}
