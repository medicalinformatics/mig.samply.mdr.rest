/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import com.google.common.collect.Lists;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.HierarchyDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dto.CodeDto;
import de.samply.mdr.dto.DefinitionDto;
import de.samply.mdr.dto.ElementDto;
import de.samply.mdr.dto.HierarchyDto;
import de.samply.mdr.dto.IdentificationDto;
import de.samply.mdr.dto.NodeDto;
import de.samply.mdr.dto.SlotDto;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * Handles all requests to catalogs.
 */
@Path("/catalogs")
public class CatalogHandler extends AbstractHandler {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/labels")
  public Collection<DefinitionDto> getLabels(@PathParam("urn") String urn)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getDefinitions(element);
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}")
  public ElementDto get(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }

      ElementDto dto = new ElementDto();
      dto.setDesignations(getDefinitions(element));
      dto.setSlots(getSlots(ctx, element.getScoped().toString()));
      dto.setIdentification(new IdentificationDto(element.getScoped()));

      return dto;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/slots")
  public Collection<SlotDto> getSlots(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getSlots(ctx, element.getScoped().toString());
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/codes")
  public HierarchyDto getAll(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }

      NodeDto rootNode = new NodeDto();

      rootNode.setIdentification(new IdentificationDto(element.getScoped()));
      rootNode.setDesignations(getDefinitions(element));

      HashMap<Integer, CodeDto> nodes = new HashMap<>();

      for (IdentifiedElement subElement : IdentifiedDao
          .getAllSubMembers(ctx, user.getId(), element.getScoped().toString())) {
        CodeDto code = new CodeDto();
        Code c = (Code) subElement.getElement();
        code.setDesignations(getDefinitions(subElement));
        code.setIdentification(new IdentificationDto(subElement.getScoped().toString()));
        code.setIsValid(c.isValid());
        code.setCode(c.getCode());
        nodes.put(subElement.getScoped().getId(), code);
      }

      for (HierarchyNode h : HierarchyDao.getHierarchyNodes(ctx, element.getScoped().toString())) {
        if (Objects.equals(h.getSuperId(), element.getScoped().getId())) {
          rootNode.getSubCodes().add(nodes.get(h.getSubId()).getCode());
        } else {
          nodes.get(h.getSuperId()).getSubCodes().add(nodes.get(h.getSubId()).getCode());
        }
      }

      HierarchyDto dto = new HierarchyDto();
      dto.setCodes(Lists.newArrayList(nodes.values()));
      dto.setRoot(rootNode);
      return dto;
    }
  }


}
