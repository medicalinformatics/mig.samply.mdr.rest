/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ComparisonDao;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Recommendation;
import de.samply.mdr.dto.CodeDto;
import de.samply.mdr.dto.ConceptAssociationDto;
import de.samply.mdr.dto.DataElementDto;
import de.samply.mdr.dto.DefinitionDto;
import de.samply.mdr.dto.HierarchyDto;
import de.samply.mdr.dto.IdentificationDto;
import de.samply.mdr.dto.SlotDto;
import de.samply.mdr.dto.UiModelDto;
import de.samply.mdr.dto.ValueDomainDto;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.string.util.StringUtil;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * Handles all dataelement requests
 */
@Path("/dataelements")
public class DataElementHandler extends AbstractHandler {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/labels")
  public Collection<DefinitionDto> getLabels(@PathParam("urn") String urn)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getDefinitions(element);
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/validations")
  public ValueDomainDto getValidations(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getValueDomain(ctx, element);
    }
  }

  /**
   * Read the full catalog with all subcodes
   *
   * @param urn the urn of the dataelement whose catalog to load in either designation or definition
   * are included
   * @return the catalog
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/catalog")
  public HierarchyDto getCatalog(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getCatalog(ctx, element);
    }
  }

  /**
   * Read the filtered catalog with all subcodes
   *
   * @param urn the urn of the dataelement whose catalog to load
   * @param query filter string. if set, only those entries that contain the query string in either
   * designation or definition are included
   * @return the catalog
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/catalog/search/{query}")
  public HierarchyDto getFilteredCatalog(@PathParam("urn") String urn,
      @PathParam("query") String query) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getCatalog(ctx, element, query);
    }
  }

  /**
   * Read the catalog node with all direct descendants
   *
   * @param urn the urn of the dataelement the catalog belongs to
   * @param code the code for which the children should be loaded
   * @return the catalog node
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/catalog/{code}/codes")
  public HierarchyDto getCatalogCodes(@PathParam("urn") String urn, @PathParam("code") String code
  ) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getCatalogCodes(ctx, element, code);
    }
  }

  /**
   * Get a code element belonging to a catalog of a given dataelement
   *
   * @param urn the urn of the dataelement the catalog belongs to
   * @param codeValue the value of the code to search
   * @return the code node or a 404
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/catalog/code")
  public CodeDto getCode(@PathParam("urn") String urn, @QueryParam("codeValue") String codeValue)
      throws DataAccessException {
    if (StringUtil.isEmpty(codeValue)) {
      throw new BadRequestException();
    }
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElementByCode(ctx, user.getId(), urn, codeValue);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }

      Code code = (Code) element.getElement();

      CodeDto dto = new CodeDto();
      dto.setDesignations(getDefinitions(element));
      dto.setSlots(getSlots(ctx, element.getScoped().toString()));
      dto.setIdentification(new IdentificationDto(element.getScoped()));
      dto.setCode(code.getCode());
      dto.setIsValid(code.isValid());

      return dto;
    }
  }

  @Deprecated
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/uimodel")
  public UiModelDto getUIModel(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      UiModelDto model = new UiModelDto();
      model.setDesignations(getDefinitions(element));
      model.setValueDomain(getValueDomain(ctx, element));
      return model;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/slots")
  public Collection<SlotDto> getSlots(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getSlots(ctx, element.getScoped().toString());
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/concepts")
  public Collection<ConceptAssociationDto> getConceptAssociations(@PathParam("urn") String urn)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getConceptAssociations(ctx, element.getScoped().toString());
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
  public DataElementDto getDataElement(@PathParam("urn") String urn) throws DataAccessException {
    /*try (DSLContext ctx = ResourceManager.getDslContext()) {*/
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getDataElementDTO(ctx, element);
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("recommendations")
  public Response getRecommendations(@QueryParam("searchedString") String searchedStr,
      @QueryParam("limit") int limit, @QueryParam("threshold") int threshold)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ComparisonDao dao = new ComparisonDao();

      if (user.getUsername() == null) {
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }

      List<Recommendation> recommendations = ComparisonDao
          .getRecommendedElements(ctx, user.getId(), searchedStr, Util.getLocale(headers), limit,
              threshold);

      return Response.ok(recommendations).build();
    }
  }

  /*
   * For now, it is not possible to create new data elements with the rest interface
   */
//	@POST
//	@Path("/new/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ScopedIdentifierDto newDataElement(DataElementDto dto, @PathParam("urn") String urn) throws MDRException {
//
//		try (MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifierDto urnDTO = ScopedIdentifierDto.parseFromURN(urn);
//			Namespace namespace = getNamespace(urnDTO.getNamespace(), mdr);
//			ValueDomainDto valueDTO = dto.getValueDomain();
//
//			ValueDomain valueDomain = convert(valueDTO);
//
//			if (valueDomain instanceof DescribedValueDomain) {
//				DescribedValueDomain described = (DescribedValueDomain) valueDomain;
//				described.setValidationType(ValidationType.NONE);
//				described.setValidationData("");
//				mdr.getDescribedDao().saveDescribedValueDomain(described);
//			} else {
//				EnumeratedValueDomain enumerated = (EnumeratedValueDomain) valueDomain;
//				mdr.getEnumeratedDao().saveEnumeratedValueDomain(enumerated);
//			}
//
//			DataElement element = new DataElement();
//			element.setValueDomainId(valueDomain.getId());
//			mdr.getDataElementDao().saveDataElement(element);
//
//			for (SlotDto slotdto : dto.getSlots()) {
//				Slot slot = new Slot();
//				slot.setKey(slotdto.getKey());
//				slot.setValue(slotdto.getValue());
//				slot.setIdentifiedId(element.getIdentifiedId());
//				mdr.getSlotDao().saveSlot(slot);
//			}
//
//			ScopedIdentifier identifier = new ScopedIdentifier();
//			identifier.setStatus(Status.RELEASED);
//			identifier.setNamespaceId(mdr.getNamespaceDao().findNamespaces()
//					.get(0).getId());
//			identifier.setVersion(urnDTO.getVersion());
//			identifier.setIdentifiedId(element.getIdentifiedId());
//			identifier.setNamespaceId(namespace.getId());
//			identifier.setNamespace(namespace.getName());
//			identifier.setIdentifier("" + urnDTO.getIdentifier());
//			identifier.setElementType(ElementType.DATAELEMENT);
//			identifier.setUrl("none");
//			identifier.setCreatedBy(1);
//
//			mdr.getScopedDao().saveScopedIdentifier(identifier);
//
//			for (DefinitionDto defdes : dto.getDesignations()) {
//				Definition def = new Definition();
//				def.setDefinition(defdes.getDefinition());
//				def.setDesignation(defdes.getDesignation());
//				def.setLanguage(defdes.getLanguage());
//				def.setDesignatableId(element.getDesignatableId());
//				def.setScopedIdentifierId(identifier.getId());
//				mdr.saveDefDesignation(def);
//			}
//
//			if (valueDomain instanceof DescribedValueDomain) {
//				for (DefinitionDto meaning : valueDTO.getMeanings()) {
//					DescribedValueDomain described = (DescribedValueDomain) valueDomain;
//					Definition def = new Definition();
//					def.setDefinition(meaning.getDefinition());
//					def.setDesignation(meaning.getDesignation());
//					def.setLanguage(meaning.getLanguage());
//					def.setDesignatableId(described.getDesignatableId());
//					def.setScopedIdentifierId(identifier.getId());
//					mdr.saveDefDesignation(def);
//				}
//			} else {
//				for (PermissibleValueDto permissible : valueDTO
//						.getPermissibleValues()) {
//					PermissibleValue value = new PermissibleValue();
//					value.setPermittedValue(permissible.getValue());
//					value.setEnumeratedValueDomainId(valueDomain.getId());
//					mdr.getPermissibleDao().savePermissibleValue(value);
//
//					for (DefinitionDto meaning : permissible.getMeanings()) {
//						Definition def = new Definition();
//						def.setDesignatableId(value.getDesignatableId());
//						def.setLanguage(meaning.getLanguage());
//						def.setDefinition(meaning.getDefinition());
//						def.setDesignation(meaning.getDesignation());
//						def.setScopedIdentifierId(identifier.getId());
//						mdr.saveDefDesignation(def);
//					}
//				}
//			}
//
//			mdr.commit();
//
//			ScopedIdentifierDto scoped = new ScopedIdentifierDto();
//			scoped.setNamespace(namespace.getName());
//			scoped.setVersion(identifier.getVersion());
//			scoped.setType("dataelement");
//			scoped.setIdentifier(identifier.getIdentifier());
//
//
//			return scoped;
//		}
//	}

}
