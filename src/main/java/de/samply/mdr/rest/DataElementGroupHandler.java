/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dto.DataElementGroupDto;
import de.samply.mdr.dto.DataElementSearchDto;
import de.samply.mdr.dto.DefinitionDto;
import de.samply.mdr.dto.IdentificationDto;
import de.samply.mdr.dto.ResultDto;
import de.samply.mdr.dto.SlotDto;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * Handles all dataelementgroup requests
 */
@Path("/dataelementgroups")
public class DataElementGroupHandler extends AbstractHandler {

  /*
   * For now, it is not possible to create new dataelementgroups with the rest interface
   */
//	@POST
//	@Path("/new/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ScopedIdentifierDto addNewGroup(DataElementGroupDto dto, @PathParam("urn") String urn)
//			throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifierDto urnDTO = ScopedIdentifierDto.parseFromURN(urn);
//			Namespace namespace = mdr.getNamespaceDao().findNamespaceByName(urnDTO.getNamespace());
//
//			DataElementGroup group = new DataElementGroup();
//
//			mdr.getDataElementGroupDao().saveDataElementGroup(group);
//
//			ScopedIdentifier identifier = new ScopedIdentifier();
//			identifier.setIdentifiedId(group.getIdentifiedId());
//			identifier.setIdentifier(urnDTO.getIdentifier());
//			identifier.setStatus(Status.RELEASED);
//			identifier.setNamespaceId(namespace.getId());
//			identifier.setNamespace(namespace.getName());
//			identifier.setElementType(ElementType.DATAELEMENTGROUP);
//			identifier.setVersion(urnDTO.getVersion());
//			identifier.setUrl("none");
//			identifier.setCreatedBy(1);
//
//			mdr.getScopedDao().saveScopedIdentifier(identifier);
//
//			for(DefinitionDto def : dto.getDesignations()) {
//				Definition definition = convert(def);
//				definition.setDesignatableId(group.getDesignatableId());
//				definition.setScopedIdentifierId(identifier.getId());
//				mdr.getDefinitionDao().saveDefinition(definition);
//			}
//
//			ScopedIdentifierDto scopedDTO = new ScopedIdentifierDto();
//			scopedDTO.setIdentifier(identifier.getIdentifier());
//			scopedDTO.setNamespace(namespace.getName());
//			scopedDTO.setType("dataelementgroup");
//			scopedDTO.setVersion(identifier.getVersion());
//
//			mdr.commit();
//
//			return scopedDTO;
//		}
//	}
//
//	@POST
//	@Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/addmember/{dataelement:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response addNewMember(@PathParam("urn") String groupURN, @PathParam("dataelement") String elementURN) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//
//			DataElementGroup group = mdr.getDataElementGroupDao().getDataElementGroupByURN(groupURN);
//			DataElement element = mdr.getDataElementDao().getDataElementByURN(elementURN);
//
//			if(group == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			if(element == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			ScopedIdentifier groupIdentifier = mdr.getScopedDao().getScopedIdentifier(groupURN);
//			ScopedIdentifier elementIdentifier = mdr.getScopedDao().getScopedIdentifier(elementURN);
//
//			mdr.getScopedDao().addSubIdentifier(groupIdentifier.getId(), elementIdentifier.getId());
//
//			mdr.commit();
//			return Response.status(200).build();
//		}
//	}
//
//	@POST
//	@Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/addsubgroup/{suburn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response addNewSubgroup(@PathParam("urn") String groupURN, @PathParam("suburn") String subGroupURN) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//
//			DataElementGroup group = mdr.getDataElementGroupDao().getDataElementGroupByURN(groupURN);
//			DataElementGroup subGroup = mdr.getDataElementGroupDao().getDataElementGroupByURN(subGroupURN);
//
//			if(group == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			if(subGroup == null) {
//				throw new ElementNotFoundException(subGroupURN);
//			}
//
//			ScopedIdentifier groupIdentifier = mdr.getScopedDao().getScopedIdentifier(groupURN);
//			ScopedIdentifier subgroupIdentifier = mdr.getScopedDao().getScopedIdentifier(subGroupURN);
//
//			mdr.getScopedDao().addSubIdentifier(groupIdentifier.getId(), subgroupIdentifier.getId());
//
//			mdr.commit();
//			return Response.status(200).build();
//		}
//	}

  @GET
  @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/members")
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto getMembers(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }

      DataElementSearchDto dto = new DataElementSearchDto();
      List<ResultDto> records = new ArrayList<>();

      // Because the URN may contain the revision "latest", replace it by the actual URN.
      for (IdentifiedElement member : IdentifiedDao
          .getSubMembers(ctx, user.getId(), element.getScoped().toString())) {
        if (member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
          continue;
        }

        ResultDto record = new ResultDto();
        record.setId(member.getScoped().toString());
        record.setDefinitions(getDefinitions(member));
        record.setIdentification(new IdentificationDto(member.getScoped()));
        records.add(record);
      }

      dto.setTotalCount(records.size());
      dto.setResults(records);

      return dto;
    }
  }

  @GET
  @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/subgroups")
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto getSubgroups(@PathParam("urn") String urn)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }

      DataElementSearchDto dto = new DataElementSearchDto();
      List<ResultDto> records = new ArrayList<>();

      // Because the URN may contain the revision "latest", replace it by the actual URN.
      for (IdentifiedElement member : IdentifiedDao
          .getSubMembers(ctx, user.getId(), element.getScoped().toString())) {
        if (member.getElementType() != Elementtype.DATAELEMENTGROUP) {
          continue;
        }

        if (member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
          continue;
        }

        ResultDto record = new ResultDto();
        record.setId(member.getScoped().toString());
        record.setDefinitions(getDefinitions(member));
        record.setIdentification(new IdentificationDto(member.getScoped()));
        records.add(record);
      }

      dto.setTotalCount(records.size());
      dto.setResults(records);

      return dto;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/slots")
  public Collection<SlotDto> getSlots(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getSlots(ctx, element.getScoped().toString());
    }
  }

  @GET
  @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/labels")
  @Produces(MediaType.APPLICATION_JSON)
  public Collection<DefinitionDto> getDefinitions(@PathParam("urn") String urn)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      return getDefinitions(element);
    }
  }

  @GET
  @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementGroupDto getGroup(@PathParam("urn") String urn) throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      IdentifiedElement element = IdentifiedDao.getElement(ctx, user.getId(), urn);
      if (element == null) {
        throw new ElementNotFoundException(urn);
      }
      DataElementGroupDto dto = new DataElementGroupDto();
      dto.setIdentification(new IdentificationDto(element.getScoped()));
      dto.setDesignations(getDefinitions(element));

      DataElementSearchDto members = new DataElementSearchDto();
      List<ResultDto> records = new ArrayList<>();

      // Because the URN may contain the revision "latest", replace it by the actual URN.
      for (IdentifiedElement member : IdentifiedDao
          .getSubMembers(ctx, user.getId(), element.getScoped().toString())) {
        if (member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
          continue;
        }

        ResultDto record = new ResultDto();
        record.setId(member.getScoped().toString());
        record.setDefinitions(getDefinitions(member));
        record.setIdentification(new IdentificationDto(member.getScoped()));
        records.add(record);
      }

      members.setTotalCount(records.size());
      members.setResults(records);

      dto.setMembers(members);
      dto.setSlots(getSlots(ctx, element.getScoped().toString()));

      return dto;
    }
  }
}
