/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest.helper;

import de.samply.auth.client.jwt.JwtAccessToken;
import de.samply.auth.rest.Scope;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.UserDao;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.rest.MdrConfig;
import de.samply.mdr.rest.exceptions.InvalidAccessTokenException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.hk2.api.Factory;
import org.jooq.DSLContext;

/**
 * Checks the user from the access token used in the header or GET parameter
 */
@Provider
public class UserFactory implements Factory<User> {

  private static final Logger logger = LogManager.getLogger(UserFactory.class);

  private User user = null;

  @Inject
  public UserFactory(HttpServletRequest request) {
    String auth = request.getHeader("Authorization");

    /**
     * In this case we ignore an invalid Access token.
     */
    try (DSLContext ctx  = ResourceManager.getDslContext()) {
      if (auth == null) {
        logger.debug("Using 'GET' parameter 'access_token'");
        auth = request.getParameter("access_token");
      } else {
        String[] inp = auth.split(" ");

        if (inp[0].equals("Bearer") && inp.length >= 2) {
          auth = auth.replaceAll("^Bearer\\s+", "").replaceAll("\\s+", "");
          logger.debug("Using Header 'Authorization'");
        } else {
          logger.debug("Access token has an invalid format");
          throw new InvalidAccessTokenException();
        }
      }

      if (auth != null) {
        JwtAccessToken accessToken = new JwtAccessToken(MdrConfig.getPublicKey(), auth);

        if (!accessToken.getScopes().contains(Scope.MDR.getIdentifier())) {
          logger.debug("AccessToken does not contain MDR scope");
          throw new InvalidAccessTokenException();
        }

        if (accessToken.isValid()) {
          logger.debug("Access token is valid.");
          String subject = accessToken.getClaimsSet().getSubject();
          user = UserDao.getUserByIdentity(ctx, subject);

          if (user == null) {
            logger.debug("Access token is valid, but the user is unknown");
          }
        } else {
          logger.debug("Access token is invalid");
          throw new InvalidAccessTokenException();
        }
      } else {
        logger.debug("No access token found, assuming anonymous access");
      }
    } catch (Exception e) {
      /**
       * Ignore the invalid access token
       */
      e.printStackTrace();
    }

    if (user == null) {
      user = new User();
      user.setId(0);
    }
  }

  @Override
  public User provide() {
    return user;
  }

  @Override
  public void dispose(User instance) {

  }

}
