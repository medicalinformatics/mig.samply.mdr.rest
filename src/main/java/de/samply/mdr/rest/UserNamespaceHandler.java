/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dto.DataElementSearchDto;
import de.samply.mdr.dto.ResultDto;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * Handles all requests regarding the namespaces of the requesting user.
 */
@Path("/")
public class UserNamespaceHandler extends AbstractHandler {

  /**
   * Returns all root elements that are in one of the writable namespaces of the requesting user.
   */
  @Path("/members")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto getRootElements() throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Collection<ResultDto> elements = new ArrayList<>();

      for (DescribedElement s : NamespaceDao.getWritableNamespaces(ctx, user.getId())) {
        elements.addAll(convert(IdentifiedDao
            .getRootElements(ctx, user.getId(), ((Namespace) s.getElement()).getName())));
      }

      DataElementSearchDto dto = new DataElementSearchDto();
      dto.setTotalCount(elements.size());
      dto.setResults(elements);
      return dto;
    }
  }

  /**
   * Searches all writable namespaces for an element by text.
   */
  @Path("/search")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto searchElements(@QueryParam("query") String query)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Collection<ResultDto> elements = new ArrayList<>();

      for (DescribedElement s : NamespaceDao.getWritableNamespaces(ctx, user.getId())) {
        elements.addAll(convert(
            IdentifiedDao
                .findElements(ctx, user.getId(), query, ((Namespace) s.getElement()).getName(),
                    new Elementtype[]{Elementtype.DATAELEMENT, Elementtype.DATAELEMENTGROUP,
                        Elementtype.RECORD},
                    new Status[]{Status.RELEASED},
                    new HashMap<String, String>())));
      }

      DataElementSearchDto dto = new DataElementSearchDto();
      dto.setTotalCount(elements.size());
      dto.setResults(elements);
      return dto;
    }
  }

  /**
   * Finds other representations of the given URN in the users namespace
   */
  @Path("/representations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto findRepresentations(@QueryParam("urn") String urn,
      @QueryParam("status") List<String> status) throws DataAccessException {
    if (!ScopedIdentifier.isUrn(urn)) {
      throw new NotFoundException();
    }

    Status[] statusEnum = new Status[status.size()];

    try {
      for (int i = 0; i < status.size(); ++i) {
        statusEnum[i] = Status.valueOf(status.get(i));
      }
    } catch (Exception e) {
      throw new BadRequestException();
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Collection<ResultDto> elements = convert(
          IdentifiedDao.findOwnedRepresentations(ctx, user.getId(), urn, statusEnum));
      DataElementSearchDto dto = new DataElementSearchDto();
      dto.setTotalCount(elements.size());
      dto.setResults(elements);
      return dto;
    }
  }

}
