/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dto.DataElementSearchDto;
import de.samply.mdr.dto.ResultDto;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * Handles all requests regarding the namespaces of the requesting user.
 */
@Path("/")
public class RootHandler extends AbstractHandler {

  /**
   * Searches all namespaces for an element by text.
   */
  @Path("/search/local")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public DataElementSearchDto search(@QueryParam("query") String query,
      @QueryParam("type") List<String> types, @QueryParam("status") List<String> statuses)
      throws DataAccessException {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<Elementtype> typeList = new ArrayList<>();
      for (String type : types) {
        typeList.add(Elementtype.valueOf(type.toUpperCase()));
      }
      Status[] statusList = new Status[statuses.size()];
      for (int i = 0; i < statuses.size(); i++) {
        statusList[i] = Status.valueOf(statuses.get(i).toUpperCase());
      }
      DataElementSearchDto dto = new DataElementSearchDto();
      Collection<ResultDto> results = convert(
          IdentifiedDao.findElements(ctx, user.getId(), query, null,
              typeList.toArray(new Elementtype[types.size()]), statusList,
              new HashMap<String, String>()));
      dto.setResults(results);
      dto.setTotalCount(results.size());
      return dto;
    }
  }

}

