/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.dao.ConceptAssociationDao;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.HierarchyDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import de.samply.mdr.dto.CodeDto;
import de.samply.mdr.dto.ConceptAssociationDto;
import de.samply.mdr.dto.DataElementDto;
import de.samply.mdr.dto.DefinitionDto;
import de.samply.mdr.dto.HierarchyDto;
import de.samply.mdr.dto.IdentificationDto;
import de.samply.mdr.dto.NamespaceDto;
import de.samply.mdr.dto.NodeDto;
import de.samply.mdr.dto.PermissibleValueDto;
import de.samply.mdr.dto.ResultDto;
import de.samply.mdr.dto.ScopedIdentifierDto;
import de.samply.mdr.dto.SlotDto;
import de.samply.mdr.dto.ValueDomainDto;
import de.samply.mdr.rest.AbstractHandler.Tree.Node;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.string.util.StringUtil;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * This abstract handler creates data transfer objects using the POJOs from the database access
 * layer.
 */
public class AbstractHandler {

  public static final String STRING_DESCRIBED_VALUE_DOMAIN = "described";
  public static final String STRING_ENUMERATED_VALUE_DOMAIN = "enumerated";
  public static final String STRING_CATALOG_VALUE_DOMAIN = "catalog";
  public static final String STRING_CATALOG_VALUE_DOMAIN_DATATYPE = "CATALOG";
  @Inject
  protected User user;
  @Context
  protected ServletContext context;
  @Context
  protected HttpHeaders headers;
  protected String rangeRegex = "(?:(.+)<=)?x(?:<=(.+))?";

  public static DefinitionDto convert(Definition d) {
    DefinitionDto dto = new DefinitionDto();
    dto.setLanguage(d.getLanguage());
    dto.setDesignation(d.getDesignation());
    dto.setDefinition(d.getDefinition());
    return dto;
  }

  public static Collection<DefinitionDto> convert(Collection<Definition> c) {
    List<DefinitionDto> target = new ArrayList<>();
    for (Definition d : c) {
      target.add(convert(d));
    }
    return target;
  }

  /**
   * Creates a DTO for an element.
   */
  protected DataElementDto getDataElementDTO(DSLContext ctx, IdentifiedElement element)
      throws DataAccessException {
    DataElementDto dto = new DataElementDto();
    dto.setSlots(getSlots(ctx, element.getScoped().toString()));
    dto.setConceptAssociations(getConceptAssociations(ctx, element.getScoped().toString()));
    dto.setDesignations(getDefinitions(element));
    dto.setValueDomain(getValueDomain(ctx, element));
    dto.setIdentification(new IdentificationDto(element.getScoped()));
    return dto;
  }

  /**
   * Creates all slot DTOs for an URN
   */
  protected Collection<SlotDto> getSlots(DSLContext ctx, String urn) throws DataAccessException {
    return SlotDto.convert(SlotDao.getSlots(ctx, urn));
  }

  /**
   * Creates all concept associations DTOs for an URN
   */
  protected Collection<ConceptAssociationDto> getConceptAssociations(DSLContext ctx, String urn)
      throws DataAccessException {
    return ConceptAssociationDto.convert(ConceptAssociationDao.getConceptAssociations(ctx, urn,user.getId()));
  }

  /**
   * Creates a value domain DTO for an element.
   */
  protected ValueDomainDto getValueDomain(DSLContext ctx, IdentifiedElement element)
      throws DataAccessException {
    DataElement de = (DataElement) element.getElement();
    List<String> locales = Util.getLocale(headers);

    if (de == null) {
      throw new ElementNotFoundException(element.getScoped().toString());
    } else {
      ValueDomain domain = (ValueDomain) ElementDao.getElement(ctx, de.getValueDomainId());
      if (domain instanceof DescribedValueDomain) {
        return convert(ctx, (DescribedValueDomain) domain, element.getScoped().getId(), locales);
      } else if (domain instanceof EnumeratedValueDomain) {
        return convert(ctx, (EnumeratedValueDomain) domain, element.getScoped().getId());
      } else if (domain instanceof CatalogValueDomain) {
        /**
         * As a workaround just handle the catalog value domain as an enumerated value domain.
         */
        return convert(ctx, (CatalogValueDomain) domain, element.getScoped().getId());
      } else {
        throw new ElementNotFoundException(element.getScoped().toString());
      }
    }
  }

  /**
   * Creates definition DTOs for an element.
   */
  protected Collection<DefinitionDto> getDefinitions(IdentifiedElement element) {
    return getDefinitions(element.getDefinitions());
  }

  /**
   * Creates definition DTOs from the specified definitions, using the locale headers. Returns
   * always a list with at least one item.
   */
  protected Collection<DefinitionDto> getDefinitions(Collection<Definition> definitions) {
    List<String> locales = Util.getLocale(headers);

    if (locales.size() == 0) {
      return convert(definitions);
    } else {
      Collection<DefinitionDto> target = new ArrayList<>();

      for (Definition def : definitions) {
        if (locales.contains(def.getLanguage())) {
          target.add(convert(def));
        }
      }

      if (target.size() == 0 && definitions.size() > 0) {
        target.add(convert(definitions.iterator().next()));
      }

      return target;
    }
  }

  /**
   * Creates a definition POJO from a dto. Was used in the methods, that create new dataelements.
   */
  protected Definition convert(DefinitionDto dto) {
    Definition d = new Definition();
    d.setDesignation(dto.getDesignation());
    d.setDefinition(dto.getDefinition());
    d.setLanguage(dto.getLanguage());
    return d;
  }

  /**
   * Searches the MDR for the specified namespace and returns it. If the namespace can not be found,
   * this method returns an ElementNotFoundException
   */
  protected Namespace getNamespace(DSLContext ctx, String name) throws DataAccessException {
    Namespace namespace = ElementDao.getNamespace(ctx, name);
    if (namespace == null) {
      throw new ElementNotFoundException("Namespace " + name);
    } else {
      return namespace;
    }
  }

  /**
   * Returns the namespace for the specified identifier.
   */
  protected Namespace getNamespace(DSLContext ctx, ScopedIdentifierDto identifier)
      throws DataAccessException {
    return getNamespace(ctx, identifier.getNamespace());
  }

  /**
   * Creates a permissible value DTO from the permissible value POJO for the specified
   * scopedIdentifier ID.
   */
  protected PermissibleValueDto convert(DSLContext ctx, PermissibleValue v, int scopedIdentifierId)
      throws DataAccessException {
    PermissibleValueDto dto = new PermissibleValueDto();

    dto.setValue(v.getPermittedValue());
    dto.setMeanings(new ArrayList<DefinitionDto>());

    dto.setMeanings(
        getDefinitions(DefinitionDao.getDefinitions(ctx, v.getId(), scopedIdentifierId)));

    return dto;
  }

  /**
   * Creates a ValueDomainDto from an enumerated value domain POJO for the specified scoped identifier ID.
   * @param en
   * @param scopedIdentifierId
   * @param ctx
   * @return
   * @throws DataAccessException
   */
  protected ValueDomainDto convert(DSLContext ctx, EnumeratedValueDomain en, int scopedIdentifierId)
      throws DataAccessException {
    ValueDomainDto dto = new ValueDomainDto();

    dto.setDatatype(en.getDatatype());
    dto.setFormat(en.getFormat());
    dto.setMaximumCharacters("" + en.getMaxCharacters());
    dto.setUnitOfMeasure(en.getUnitOfMeasure());

    dto.setPermissibleValues(new ArrayList<PermissibleValueDto>());
    for (Element v : ElementDao.getPermissibleValues(ctx, en.getId())) {
      dto.getPermissibleValues().add(convert(ctx, (PermissibleValue) v, scopedIdentifierId));
    }

    dto.setValueDomainType(STRING_ENUMERATED_VALUE_DOMAIN);

    return dto;
  }

  /**
   * Creates a new value domain DTO for the given catalog value domain.
   */
  protected ValueDomainDto convert(DSLContext ctx, CatalogValueDomain domain, int id)
      throws DataAccessException {
    ValueDomainDto dto = new ValueDomainDto();

    dto.setDatatype(STRING_CATALOG_VALUE_DOMAIN_DATATYPE);
    dto.setFormat(STRING_CATALOG_VALUE_DOMAIN);
    dto.setValueDomainType(STRING_CATALOG_VALUE_DOMAIN);
    dto.setValidationType(STRING_CATALOG_VALUE_DOMAIN_DATATYPE);

    dto.setUnitOfMeasure(null);
    return dto;
  }

  /**
   * Converts the given code into a PermissibleValue. Only used in the v2 REST interface.
   */
  protected PermissibleValueDto convertCode(IdentifiedElement element) {
    PermissibleValueDto dto = new PermissibleValueDto();
    Code code = (Code) element.getElement();
    dto.setValue(code.getCode());
    dto.setMeanings(getDefinitions(element));
    return dto;
  }

  /**
   * Returns the Catalog in the context of the given dateelement. The output may be filtered
   * (removed empty branches of the hierarchy).
   */
  protected HierarchyDto getCatalog(DSLContext ctx, IdentifiedElement element)
      throws DataAccessException {
    return getCatalog(ctx, element, null);
  }

  /**
   * Returns the Catalog in the context of the given dateelement. The output may be filtered
   * (removed empty branches of the hierarchy).
   *
   * @param query optional search String to filter by
   */
  protected HierarchyDto getCatalog(DSLContext ctx, IdentifiedElement element, String query)
      throws DataAccessException {
    HierarchyDto dto = new HierarchyDto();
    DataElement dataelement = (DataElement) element.getElement();

    Element valueDomain = ElementDao.getElement(ctx, dataelement.getValueDomainId());

    if (!(valueDomain instanceof CatalogValueDomain)) {
      throw new BadRequestException();
    }

    CatalogValueDomain catalogValueDomain = (CatalogValueDomain) valueDomain;

    /**
     * The catalog, the list of permissible codes in this catalog value domain,
     * the hierarchy nodes for this catalog and the list of all codes in this catalog.
     */
    IdentifiedElement catalog = IdentifiedDao
        .getElement(ctx, user.getId(), catalogValueDomain.getCatalogScopedIdentifierId());
    List<IdentifiedElement> permissibleCodes = IdentifiedDao
        .getPermissibleCodes(ctx, user.getId(), catalogValueDomain);
    List<HierarchyNode> nodes = HierarchyDao.getHierarchyNodes(ctx, catalog.getScoped().toString());

    List<IdentifiedElement> codes = IdentifiedDao
        .getAllSubMembers(ctx, user.getId(), catalog.getScoped().toString());

    Map<Integer, IdentifiedElement> permissibleCodeMap = new HashMap<>();
    Map<Integer, Node<CodeDto>> nodeMap = new HashMap<>();

    Tree<CodeDto> tree = new Tree<>(new CodeDto());
    tree.root.data.setIsValid(false);

    nodeMap.put(catalog.getScoped().getId(), tree.root);

    /**
     * Put all permissible codes into a separate map, if a filter term was supplied, also use that
     */
    for (IdentifiedElement code : permissibleCodes) {
      if (!StringUtil.isEmpty(query)) {
        if (!Util.definitionsContainString(code.getDefinitions(), query)) {
          continue;
        }
      }
      permissibleCodeMap.put(code.getScoped().getId(), code);
    }

    /**
     * If a code from the catalog is not in the map of permissible codes,
     * it is not a valid in the context of the dataelement. For each code
     * create a new node and put it in the map.
     */
    for (IdentifiedElement code : codes) {
      if (!permissibleCodeMap.containsKey(code.getScoped().getId())) {
        ((Code) code.getElement()).setValid(false);
      }

      CodeDto codeDTO = new CodeDto();
      Code c = (Code) code.getElement();
      codeDTO.setDesignations(getDefinitions(code));
      codeDTO.setIdentification(new IdentificationDto(code.getScoped()));
      codeDTO.setIsValid(c.isValid());
      codeDTO.setCode(c.getCode());
      nodeMap.put(code.getScoped().getId(), new Node<>(codeDTO));
    }

    /**
     * Build the tree using the hierarchy nodes and the nodeMap
     */
    for (HierarchyNode node : nodes) {
      Node<CodeDto> childNode = nodeMap.get(node.getSubId());
      Node<CodeDto> parentNode = nodeMap.get(node.getSuperId());
      parentNode.children.add(childNode);
      parentNode.data.setHasSubcodes(true);
    }

    /**
     * Removes invalid codes and filters the tree, meaning it removes "empty" branches with no valid codes and
     */
    filter(tree.root, true);

    /**
     * Reduce the tree to the node, that represents a valid code or has more than
     * one child.
     */
    reduce(tree);

    List<CodeDto> codeList = new ArrayList<>();

    /**
     * Create a list for the REST interface using the current tree.
     */
    toList(codeList, tree.root.children);
    dto.setCodes(codeList);

    dto.setRoot(new NodeDto());
    dto.getRoot().setDesignations(getDefinitions(catalog));
    dto.getRoot().setIdentification(new IdentificationDto(catalog.getScoped().toString()));
    dto.getRoot().setSubCodes(toCodeList(tree.root.children));
    return dto;
  }

  /**
   * Get a list of direct descendants of the given code in context of the given dataelement
   *
   * @param element the dataelement with the catalog as value domain
   * @param nodeCode the urn of the code to get the subcodes for
   */
  protected HierarchyDto getCatalogCodes(DSLContext ctx, IdentifiedElement element, String nodeCode)
      throws DataAccessException {
    IdentifiedElement node;
    HierarchyDto dto = new HierarchyDto();
    DataElement dataelement = (DataElement) element.getElement();

    Element valueDomain = ElementDao.getElement(ctx, dataelement.getValueDomainId());

    if (!(valueDomain instanceof CatalogValueDomain)) {
      throw new BadRequestException();
    }

    CatalogValueDomain catalogValueDomain = (CatalogValueDomain) valueDomain;

    /**
     * The catalog, the list of permissible codes in this catalog value domain,
     * the hierarchy nodes for this catalog and the list of all codes in this catalog.
     */
    IdentifiedElement catalog = IdentifiedDao
        .getElement(ctx, user.getId(), catalogValueDomain.getCatalogScopedIdentifierId());
    List<IdentifiedElement> permissibleCodes = IdentifiedDao
        .getPermissibleCodes(ctx, user.getId(), catalogValueDomain);

    if ("root".equalsIgnoreCase(nodeCode)) {
      node = catalog;
    } else {
      node = IdentifiedDao.getElement(ctx, user.getId(), nodeCode);
    }

    List<HierarchyNode> nodes = HierarchyDao
        .getHierarchyNodes(ctx, node.getScoped().toString(), true);

    /**
     * Check which of these sub-codes are listed as super-codes as well
     */
    List<Integer> childIds = new ArrayList<>();
    for (HierarchyNode hn : nodes) {
      childIds.add(hn.getSubId());
    }
    Map<Integer, Boolean> nodesWithChildren = HierarchyDao
        .getNodesWithChildren(ctx, catalogValueDomain.getId(), childIds);

    List<IdentifiedElement> codes = IdentifiedDao
        .getChildren(ctx, user.getId(), node.getScoped().toString());

    Map<Integer, IdentifiedElement> permissibleCodeMap = new HashMap<>();
    Map<Integer, Node<CodeDto>> nodeMap = new HashMap<>();

    Tree<CodeDto> tree = new Tree<>(new CodeDto());
    tree.root.data.setIsValid(false);

    nodeMap.put(node.getScoped().getId(), tree.root);

    /**
     * Put all permissible codes into a separate map, if a filter term was supplied, also use that
     */
    for (IdentifiedElement code : permissibleCodes) {
      permissibleCodeMap.put(code.getScoped().getId(), code);
    }

    /**
     * If a code from the catalog is not in the map of permissible codes,
     * it is not a valid in the context of the dataelement. For each code
     * create a new node and put it in the map.
     */
    for (IdentifiedElement code : codes) {
      if (!permissibleCodeMap.containsKey(code.getScoped().getId())) {
        ((Code) code.getElement()).setValid(false);
      }

      CodeDto codeDTO = new CodeDto();
      Code c = (Code) code.getElement();
      codeDTO.setDesignations(getDefinitions(code));
      codeDTO.setIdentification(new IdentificationDto(code.getScoped()));
      codeDTO.setIsValid(c.isValid());
      codeDTO.setCode(c.getCode());
      nodeMap.put(code.getScoped().getId(), new Node<>(codeDTO));
    }

    /**
     * Build the tree using the hierarchy nodes and the nodeMap
     */
    for (HierarchyNode hierarchyNode : nodes) {
      Node<CodeDto> childNode = nodeMap.get(hierarchyNode.getSubId());
      childNode.data.setHasSubcodes(nodesWithChildren.get(hierarchyNode.getSubId()));
      nodeMap.get(hierarchyNode.getSuperId()).children.add(childNode);
    }

    /**
     * Removes invalid codes and filters the tree, meaning it removes "empty" branches with no valid codes and
     */
    filter(tree.root, false);

    /**
     * Reduce the tree to the node, that represents a valid code or has more than
     * one child.
     */
    reduce(tree);

    List<CodeDto> codeList = new ArrayList<>();

    /**
     * Create a list for the REST interface using the current tree.
     */
    toList(codeList, tree.root.children);
    dto.setCodes(codeList);

    dto.setRoot(new NodeDto());
    dto.getRoot().setDesignations(getDefinitions(node));
    dto.getRoot().setIdentification(new IdentificationDto(node.getScoped().toString()));
    dto.getRoot().setSubCodes(toCodeList(tree.root.children));
    return dto;
  }

  /**
   *
   */
  private void toList(List<CodeDto> codeList, List<Node<CodeDto>> children) {
    for (Node<CodeDto> node : children) {
      node.data.setSubCodes(toCodeList(node.children));
      codeList.add(node.data);

      toList(codeList, node.children);
    }
  }

  /**
   * Returns a List of all sub codes as strings.
   */
  private List<String> toCodeList(List<Node<CodeDto>> children) {
    List<String> target = new ArrayList<>();
    for (Node<CodeDto> code : children) {
      target.add(code.data.getCode());
    }
    return target;
  }

  /**
   * Reduces the given tree.
   */
  private void reduce(Tree<CodeDto> tree) {
    tree.root = reduce(tree.root);
  }

  /**
   * Reduces the given node and returns the new node.
   */
  private Node<CodeDto> reduce(Node<CodeDto> node) {
    if (node.data.getIsValid()) {
      return node;
    }

    if (node.children.size() > 1) {
      return node;
    }

    if (node.children.size() == 1 && !node.children.get(0).data.getIsValid() &&
        !node.children.get(0).data.getHasSubcodes()) {
      return reduce(node.children.get(0));
    }

    return node;
  }

  /**
   * Removed invalid codes without children.
   *
   * @param node the node to filter
   * @param ignoreSubcodes if true, the "hasSubcodes" value of a node will be ignored when
   * filtering. This should be used when getting non-dynamic catalogues
   */
  private void filter(Node<CodeDto> node, boolean ignoreSubcodes) {
    Iterator<Node<CodeDto>> iterator = node.children.iterator();
    while (iterator.hasNext()) {
      Node<CodeDto> next = iterator.next();

      filter(next, ignoreSubcodes);

      if (!next.data.getIsValid() && next.children.size() == 0 &&
          (!next.data.getHasSubcodes() || ignoreSubcodes)) {
        iterator.remove();
      }
    }
  }

  /**
   * Creates a value domain POJO for the specified DTO. Was used in the methods, that create new
   * dataelements.
   */
  protected ValueDomain convert(ValueDomainDto valueDTO) {
    ValueDomain domain;
    if (valueDTO.getValueDomainType().equals(STRING_DESCRIBED_VALUE_DOMAIN)) {
      DescribedValueDomain described = new DescribedValueDomain();
      described.setDescription(valueDTO.getDescription());
      domain = described;
    } else {
      domain = new EnumeratedValueDomain();
    }

    domain.setDatatype(valueDTO.getDatatype());
    domain.setFormat(valueDTO.getFormat());
    domain.setMaxCharacters(Integer.parseInt(valueDTO.getMaximumCharacters()));
    domain.setUnitOfMeasure(valueDTO.getUnitOfMeasure());

    return domain;
  }

  /**
   * Creates a value domain DTO from the specified described value domain POJO for the specified
   * scoped identifier ID and locales.
   */
  protected ValueDomainDto convert(DSLContext ctx, DescribedValueDomain va, int scopedIdentifierId,
      List<String> locales) throws DataAccessException {
    ValueDomainDto dto = new ValueDomainDto();

    dto.setDatatype(va.getDatatype());
    dto.setDescription(va.getDescription());
    dto.setFormat(va.getFormat());
    dto.setMaximumCharacters("" + va.getMaxCharacters());
    dto.setUnitOfMeasure(va.getUnitOfMeasure());
    dto.setValidationType(va.getValidationType().toString());
    dto.setValidationData(va.getValidationData());
    dto.setErrorMessages(getErrorMessages(va, locales));

    dto.setMeanings(
        getDefinitions(DefinitionDao.getDefinitions(ctx, va.getId(), scopedIdentifierId)));
    dto.setValueDomainType(STRING_DESCRIBED_VALUE_DOMAIN);

    return dto;
  }

  /**
   * Returns a list of error messages for the specified described value domain and locales.
   */
  private Collection<DefinitionDto> getErrorMessages(DescribedValueDomain va,
      Collection<String> locales) {
    Collection<DefinitionDto> target = new ArrayList<>();

    if (va.getValidationType() == Validationtype.NONE) {
      return Collections.emptyList();
    }

    for (String locale : locales) {
      ResourceBundle bundle = ResourceBundle.getBundle("bundles/Messages", new Locale(locale));
      if (bundle != null && bundle.getLocale().getLanguage().equals(locale)) {
        DefinitionDto dto = getErrorMessage(bundle, va, locale);
        dto.setLanguage(bundle.getLocale().getLanguage());
        target.add(dto);
      }
    }

    if (target.size() == 0) {
      ResourceBundle bundle = ResourceBundle.getBundle("bundles/Messages", new Locale("en"));
      DefinitionDto dto = getErrorMessage(bundle, va, "en");
      dto.setLanguage(bundle.getLocale().getLanguage());
      target.add(dto);
    }

    return target;
  }

  /**
   * Creates an error message from the specified bundle for the specified value domain and locale.
   */
  private DefinitionDto getErrorMessage(ResourceBundle bundle, DescribedValueDomain va,
      String locale) {
    DefinitionDto dto = new DefinitionDto();
    Pattern pattern = Pattern.compile(rangeRegex);
    Matcher matcher = pattern.matcher(va.getValidationData());
    switch (va.getValidationType()) {
      case BOOLEAN:
        dto.setDefinition(bundle.getString("boolean_definition"));
        dto.setDefinition(bundle.getString("boolean_designation"));
        break;

      case INTEGER:
        dto.setDefinition(bundle.getString("integer_definition"));
        dto.setDesignation(bundle.getString("integer_designation"));
        break;

      case FLOAT:
        dto.setDefinition(bundle.getString("float_definition"));
        dto.setDesignation(bundle.getString("float_designation"));
        break;

      case NONE:
        dto.setDefinition(bundle.getString("none_definition"));
        dto.setDesignation(bundle.getString("none_designation"));
        break;

      case REGEX:
        dto.setDefinition(
            MessageFormat.format(bundle.getString("regex_definition"), va.getValidationData()));
        dto.setDesignation(bundle.getString("regex_designation"));
        break;

      case DATE:
        dto.setDefinition(bundle.getString("date_definition"));
        dto.setDesignation(bundle.getString("date_designation"));
        break;

      case TIME:
        dto.setDefinition(bundle.getString("time_definition"));
        dto.setDesignation(bundle.getString("time_designation"));
        break;

      case DATETIME:
        dto.setDefinition(bundle.getString("datetime_definition"));
        dto.setDesignation(bundle.getString("datetime_designation"));
        break;

      case FLOATRANGE:
        if (matcher.find()) {
          String first = matcher.group(1);
          String second = matcher.group(2);

          if (first != null && second != null) {
            dto.setDefinition(MessageFormat
                .format(bundle.getString("float_range_both_limits_definition"), first, second));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else if (first == null && second != null) {
            dto.setDefinition(
                MessageFormat.format(bundle.getString("float_range_upper_limit_definition"), second));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else if (first != null && second == null) {
            dto.setDefinition(
                MessageFormat.format(bundle.getString("float_range_lower_limit_definition"), first));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else {
            dto.setDefinition(bundle.getString("float_definition"));
            dto.setDesignation(bundle.getString("float_designation"));
          }
        } else {
          // At this point something has gone horribly wrong (data corruption in the database).
          // It does not make sense to give an invalid error message.
        }
        break;
      case INTEGERRANGE:
        if (matcher.find()) {
          String first = matcher.group(1);
          String second = matcher.group(2);

          if (first != null && second != null) {
            dto.setDefinition(MessageFormat
                .format(bundle.getString("integer_range_both_limits_definition"), first, second));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else if (first == null && second != null) {
            dto.setDefinition(
                MessageFormat.format(bundle.getString("integer_range_upper_limit_definition"), second));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else if (first != null && second == null) {
            dto.setDefinition(
                MessageFormat.format(bundle.getString("integer_range_lower_limit_definition"), first));
            dto.setDesignation(bundle.getString("range_invalid"));
          } else {
            dto.setDefinition(bundle.getString("integer_definition"));
            dto.setDesignation(bundle.getString("integer_designation"));
          }
        } else {
          // At this point something has gone horribly wrong (data corruption in the database).
          // It does not make sense to give an invalid error message.
        }
        break;

      case JS:
        break;
      case LUA:
        break;
      default:
        break;

    }
    return dto;
  }

  /**
   * Creates a list of namespace DTOs from the specified readable namespace list and writable
   * namespace list.
   */
  protected Collection<NamespaceDto> convert(List<DescribedElement> readable,
      List<DescribedElement> writable) {
    HashSet<String> writableHash = new HashSet<>();

    for (DescribedElement ns : writable) {
      if (ns.getElement() instanceof Namespace) {
        Namespace n = (Namespace) ns.getElement();
        writableHash.add(n.getName());
      }
    }

    List<NamespaceDto> target = new ArrayList<>();

    for (DescribedElement ns : readable) {
      if (ns.getElement() instanceof Namespace) {
        Namespace n = (Namespace) ns.getElement();
        NamespaceDto dto = new NamespaceDto();
        dto.setName(n.getName());
        dto.setDefinitions(getDefinitions(ns.getDefinitions()));
        dto.setWritable(writableHash.contains(n.getName()));
        target.add(dto);
      }
    }

    return target;
  }

  protected Collection<ResultDto> convert(List<IdentifiedElement> elements) {
    List<ResultDto> target = new ArrayList<>();

    for (IdentifiedElement element : elements) {
      Elementtype elementType = element.getElementType();

      if (!filter(elementType)) {
        ResultDto record = new ResultDto();
        record.setId(element.getScoped().toString());
        record.setDefinitions(getDefinitions(element));
        record.setIdentification(new IdentificationDto(element.getScoped()));
        target.add(record);
      }
    }
    return target;
  }

  protected boolean filter(Elementtype type) {
    switch (type) {
      case DATAELEMENT:
      case DATAELEMENTGROUP:
      case RECORD:
        return false;

      default:
        return true;
    }
  }

  protected static class Tree<T> {

    private Node<T> root;

    public Tree(T rootData) {
      root = new Node<>(null);
      root.data = rootData;
      root.children = new ArrayList<>();
    }

    public static class Node<T> {

      private T data;
      private List<Node<T>> children = new ArrayList<>();

      public Node(T data) {
        this.data = data;
      }
    }
  }

}
