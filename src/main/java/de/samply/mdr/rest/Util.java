/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.rest;

import de.samply.mdr.dal.dto.Definition;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.core.HttpHeaders;

public class Util {

  /**
   * A util to get the "Accept-Language" headers into a list of strings.
   */
  public static List<String> getLocale(HttpHeaders headers) {
    List<String> locale = new ArrayList<>();
    for (Locale l : headers.getAcceptableLanguages()) {
      if (l.getLanguage() != null && !l.getLanguage().equals("null") && !locale
          .contains(l.getLanguage())) {
        locale.add(l.getLanguage());
      }
    }
    return locale;
  }

  /**
   * Check all definitions (designation and definition) for the search query
   *
   * The search is NOT case-sensitive
   *
   * @param definitions collection of definitions to check
   * @param query the string to look for
   * @return true if one of the definitions designation or definition contains that string
   */
  public static boolean definitionsContainString(Collection<Definition> definitions, String query) {
    for (Definition definition : definitions) {
      if (definition.getDesignation().toLowerCase().contains(query.toLowerCase())) {
        return true;
      } else if (definition.getDefinition().toLowerCase().contains(query.toLowerCase())) {
        return true;
      }
    }
    return false;
  }

}
